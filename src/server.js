const express = require('express');
const app = express();

const cors = require('cors');

let PORT = process.env.PORT || 3000;

const routes = require('./api/routes');

const pubsubConnection = require('./lib/pubsub/connection');

pubsubConnection.connectToRabbit();

app.use(cors());
app.use(
    express.urlencoded({
        extended: true
    })
);
app.use(express.json());
routes(app);

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});