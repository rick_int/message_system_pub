const rabbit = require('../service/pubsub');

let controllers = {
    insertMessage: async (req, res) => {
        let { messageType } = req.query;
        let response = { message: 'success' };

        try {
            if (messageType === 'topic') {

                let { exchange, topic, msg } = req.body;
                await rabbit.publishMessageToExchange(exchange, topic, msg);

            } else if (messageType === 'log') {

                let { queue, log } = req.body;
                await rabbit.publishLog(queue, log);

            } else {
                response = { error: 'invalid message type' };
            }
            res.send(response);
        } catch (error) {
            res.send({
                error: error.message
            });
        }
    }
};

module.exports = controllers;