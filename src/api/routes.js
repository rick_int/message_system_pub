const controller = require('./controller');

module.exports = (app) => {
    app.route('/api/v1/publisher/message')
        .post(controller.insertMessage);
}