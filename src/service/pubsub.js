const rabbitConnection = require('../lib/pubsub/connection');

let rabbit = {
    publishMessageToExchange: async (exchange, topic, msg) => {
        try {
            let channel = await rabbitConnection.createChannel();
            await channel.assertExchange(exchange, 'topic', {
                durable: false
            });
            await channel.publish(exchange, topic, Buffer.from(msg));
            await channel.close();

            console.log(" [x] Sent %s: '%s'", topic, msg);

            return true;
        } catch (error) {
            throw error;
        }
    },
    publishLog: async (queue, log) => {
        try {
            // let queue = 'worker_log';
            let channel = await rabbitConnection.createChannel();
            await channel.assertQueue(queue, {
                durable: true
            });

            await channel.sendToQueue(queue, Buffer.from(log), {
                persistent: true
            });
            await channel.close();

            console.log(" [x] Sent '%s'", log);

            return true;
        } catch (error) {
            throw error;
        }
    }
};

module.exports = rabbit;