const rabbit = require('amqplib');

let connection = null;

const connectToRabbit = async (pubsubOptions = {}) => {

    const {
        host = process.env.RABBIT_URL || 'amqp://localhost' 
    } = pubsubOptions;

    if (!connection) {
        try {
            const conn = await rabbit.connect(host);
            connection = conn;
    
            console.log("PUB APP Connected to rabbitmq. Ready to accept messages.");
            console.log(" ## Endpoint: /api/v1/publisher/message");
            console.log(" ## Query param: messageType=log");
            console.log(" ## Body: queue=worker_log, log=<string>")
        } catch (error) {
            console.log('Error during rabbitmq connection: %s', error.message);
            
            setTimeout(() => {
                connectToRabbit();
            }, 10 * 1000); 
        }
    }
};

let createChannel = async () => {
    const rabbitChannel = await connection.createChannel();
    return rabbitChannel;
};

const closeConnection = async () => {
    await connection.close();
    return true;
};

module.exports = { connectToRabbit, createChannel, closeConnection };

