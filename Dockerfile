FROM node:12-alpine

RUN mkdir /pub/

WORKDIR /pub/

COPY ./ /pub/
RUN npm install

EXPOSE 3000
ENTRYPOINT [ "node", "src/server.js" ]